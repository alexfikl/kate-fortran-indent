var katescript = {
    "name": "Fortran",
    "author": "Alex Fikl <alexfikl@gmail.com>",
    "license": "LGPL",
    "revision": 1,
    "kate-version": "5.1",
    "indent-languages": [ "Fortran" ]
}; // kate-script-header, must be at the start of the file without comments, pure json

/**
 * Copyright (C) 2015 Alex Fikl <alexfikl@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

// required katepart js libraries
require("range.js");
require("string.js");
require("utils.js");

//BEGIN USER CONFIGURATION
// Enable dbg statements in code
var debugMode = true;

// Indent case statements inside a select
var cfgIndentCase = false;

// indent contains with respect to its parent
var cfgIndentContains = false;

// Start line with a comment if previous line contained one
var cfgContinueComment = true;

// Add spaces around certain operators
var cfgOperatorSpacing = true;
//END USER CONFIGURATION

//BEGIN REGEXP
// Force some keywords to always be on column 0
var rxIndentZero = /^\s*(program|module)\b/i;

// Regexp for function declarations (indent after)
var rxIndentFunction = /^\s*(integer|logical|real|.*\(.*\))?\s*function/i;

// Regexp to recognize if statements (indent after)
var rxIndentIf = /^\s*(else)?\s*if.*(then$)?/i;

// Regexp to recognize type definitions and not type declarations (indent after)
var rxIndentType = /^\s*type\s*(,.*|::)?\s*\w+/i;

// Regexp to recognize different case statements (indent after)
var rxIdentCase = /^\s*(case|type\s+is|class\s+default)\b/i

// Regexp for other declarations (indent after)
var rxIndentStmt = /^\s*(elemental|pure|recursive|function|subroutine|do|else|abstract|interface|forall|where|elsewhere)\b/i;

// Regexp for unindenting (unindent after)
var rxUnindentAfter = /^\s*(return|cycle|break|continue)\b/i;

// Regexp for unindenting a line
var rxUnindentStmt = /^\s*(end|else|elsewhere)/i;

//END REGEXP

//BEGIN EXTRA TRIGGERS
// characters which should trigger a reindent, besides the default '\n'
// #        for preprocessor directives
// *        different operators
triggerCharacters = "#+-*/=<>,:";
//END EXTRA TRIGGERS

/**
 * @brief Check if a line has a continuation.
 *
 * In Fortran, line continuations are marked with the & character as the
 * last non-comment character.
 *
 * @param[in] lineno        Current line number.
 */
function isLineContinuation(lineno) {
    if (lineno < 0) {
        return false;
    }

    return (/\&$/).test(stripComment(lineno));
}

/**
 * @brief Check if a given line is considered "code".
 *
 * A line is not code if it is a comment or if it is part of a continuation.
 *
 * NOTE: Kate seems to think "contains" is a comment when it's on column 0.
 */
function notCode(lineno) {
    if (document.line(lineno).trim().startsWith("contains")) {
        return false;
    }

    return document.isComment(lineno, document.firstColumn(lineno)) ||
           isLineContinuation(lineno - 1);
}

/**
 * @brief Return closest normal code line.
 *
 * A normal code line is a code line that isn't part of a continuation (these are
 * indented separately), is not a comment and is not an empty line.
 */
function findPrevCodeLine(lineno)
{
  lineno = document.prevNonEmptyLine(lineno);

  while (lineno >= 0 && notCode(lineno)) {
    lineno = document.prevNonEmptyLine(lineno - 1);
  }

  return lineno;
}

/**
 * @brief Check if a line should be unindented.
 *
 * Criteria for unindenting:
 *  * reached end statement.
 *  * finished a continuation.
 *
 * @param[in] lineno        Current line number.
 */
function shouldUnindent(lineno) {
    var lineUnindent = rxUnindentStmt.test(document.line(lineno)),
        prevLineUnindent = rxUnindentAfter.test(document.line(lineno - 1));

    return (lineUnindent && !prevLineUnindent) || prevLineUnindent;
}

/** See also, the Kickstarter campaign to update it:
 * @brief Check if a line should be indented.
 *
 * We check that the previous code line is a line that containes
 * a statement that required indentation.
 *
 * @param[in] lineno        Line number.
 * @return                  True if the line should be indented.
 */
function shouldIndent(lineContents) {
    return rxIndentStmt.test(lineContents) ||
           (lineContents.startsWith("select") && cfgIndentCase) ||
           (lineContents.startsWith("contains") && !cfgIndentContains) ||
           rxIndentIf.test(lineContents) ||
           rxIndentFunction.test(lineContents) ||
           rxIdentCase.test(lineContents) ||
           rxIndentType.test(lineContents) ||
           rxIndentZero.test(lineContents);
}

// LineContext class
function LineContext(lineno, indentWidth, ch) {
    this.indentWidth = indentWidth;
    this.number = lineno;
    this.contents = document.line(lineno).trim();
    this.ch = ch;
    this.cursor = 0;

    if (!this.contents) {
        this.contents = '';
    }

    if (ch === '\n' || ch === '') {
        this.prev = findPrevCodeLine(lineno - 1);
        this.prevContents = document.line(this.prev).trim();
        this.prevIndent = document.firstVirtualColumn(this.prev);
    } else {
        this.prev = 0;
        this.prevContents = "";
        this.prevIndent = 0;
        this.cursor = view.cursorPosition();
    }

    this.debug = function() {
        dbg('\nwidth <' + this.indentWidth + '> prevIndent <' + this.prevIndent + '>')
        dbg('line  <' + this.number + '> text: <' + this.contents + '>');
        dbg('prev  <' + this.prev + '> text: <' + this.prevContents + '>');
        if (this.cursor) {
            dbg('cursor: ' + this.cursor);
        }
    };

    this.contains = function(str) {
        return this.contents.contains(str);
    };

    this.startsWith = function(str) {
        return this.contents.startsWith(str);
    };
}

/**
 * @brief If the previous line is commented, add a comment to the new one too.
 *
 * @param[in] cursor            Cursor.
 */
function tryComment(line) {
    var prevLineContents = document.line(line.number - 1).ltrim(),
        column = document.firstVirtualColumn(line.number - 1);

    dbg('tryComment: start');
    if (!cfgContinueComment) {
        dbg('tryComment: do not auto-insert comments');
        return;
    }

    document.editBegin();

    // see which type of comment the previous line contained and insert it
    // at the current position
    if (prevLineContents.startsWith("!!")) {
        document.insertText(line.number, column, "!! ");
    } else if (prevLineContents.startsWith("!>")) {
        document.insertText(line.number, column, "!> ");
    } else if (prevLineContents.startsWith("!")) {
        document.insertText(line.number, column, "! ");
    }

    document.editEnd();
}

/**
 * @brief Indent a newline to an open brace on previous line.
 *
 * This function will handle the case where we want a multiline function call
 * like:
 * @code
 *  call function(a_long, list, of, arguments, could, be,   &
 *                given, to, this_function, foo)
 * @endcode
 * or declarations such as:
 * @code
 *  integer, dimensions(imin:imax,  &
 *                      jmin:jmax,  &
 *                      kmin:kmax) :: value
 * @endcode
 *
 * @param[in] cursor            Cursor.
 * @param[in] indentWidth       Indent width in spaces.
 */
function tryAlignOpenBrace(line) {
    var result = -1,
        lineno = line.number;
        lineContents = "",
        braces = 0,
        i = 0;

    dbg('tryAlignOpenBrace: start');
    dbg('tryAlignOpenBrace: line <' + lineno + '> prev text <' + document.line(lineno - 1) + '>');

    // if the previous line isn't a continuation, we don't care
    if (!isLineContinuation(lineno - 1)) {
        return result;
    }

    // go up in lines and count the braces to find the last one that isn't
    // closed yet
    while (isLineContinuation(lineno - 1)) {
        lineno = lineno - 1;

        // look at the braces on the current line
        lineContents = document.line(lineno);
        dbg("tryAlignOpenBrace: line <" + lineno + "> text <" + lineContents + ">");
        for (i = lineContents.length - 1; i >= 0; i -= 1) {
            if (lineContents[i] === '(') {
                // if we found an open brace and we haven't seen a closed
                // one before, it means we have found the last unclosed brace
                if (braces === 0) {
                    return i + 1;
                }

                braces -= 1;
            } else if (lineContents[i] === ')') {
                braces += 1;
            }
        }
    }

    return result;
}

/**
 * @brief Handle indentation in a select statement.
 */
function trySelect(line) {
    var result = -1,
        lineno = line.number, lineContents = "";

    dbg('trySelect: start');

    // test if previous line is a select
    if (line.prevContents.startsWith("select")) {
        dbg('trySelect: previous is select');
        if (cfgIndentCase) {
            result = document.firstVirtualColumn(lineno - 1) + line.indentWidth;
        }

        return result;
    }

    if (line.startsWith('end select') && cfgIndentCase) {
        return document.firstVirtualColumn(lineno - 1) - 2 * line.indentWidth;
    }

    // check if line is a case statement
    if (!(line.startsWith("case ") ||
          line.startsWith("type is ") ||
          line.startsWith("class default"))) {
        dbg('trySelect: line not a case');
        return result;
        }

    // the current line is a case statement: find the corresponding select
    do {
        lineno = lineno - 1;
        lineContents = document.line(lineno);
    } while(rxIndentSelect.test(lineContents) == 0);

    dbg('trySelect: found select at <' + lineno + '>');
    result = document.firstVirtualColumn(lineno);
    if (cfgIndentCase) {
        result += line.indentWidth;
    }

    return result;
}

/**
 * @brief Indent after certain keywords.
 *
 * `program` and `module` are always on column 0. `contains` can be indented with the
 * or not with respect to the parent. All other statements cause the next line
 * to be indented.
 */
function tryKeywords(line) {
    dbg('tryKeywords: start');

    if (rxIndentZero.test(line.contents)) {
        dbg("tryKeywords: zero indent");
        return 0;
    }

    if (line.startsWith("contains")) {
        dbg("tryKeywords: line starts with contains");
        if (cfgIndentContains) {
            return line.prevIndent;
        } else {
            return line.prevIndent - line.indentWidth;
        }
    }

    if (shouldIndent(line.prevContents)) {
        dbg("tryKeywords: previous line is a statement");
        return line.prevIndent + line.indentWidth;
    }

    return -1;
}

/**
 * @brief If previous line was a continuation (but no braces), use its indentation.
 *
 * @param[in] cursor        Cursor.
 */
function tryContinuation(line) {
    var prevLine = 0;

    dbg('tryContinuation: start');

    if(isLineContinuation(line.number - 1)) {
        dbg('tryContinuation: previous line is continuation');
        return document.firstVirtualColumn(line.number - 1);
    }

    // unindent if we previously indented because of a continuation
    if (isLineContinuation(line.number - 2)) {
        prevLine = findPrevCodeLine(line.number - 2);
        dbg('tryContinuation: continuation begins at <' + prevLine + '>');
        return document.firstVirtualColumn(prevLine);
    }

    return -1;
}

/**
 * @brief Compute the indentation width for a given line.
 *
 * @param[in] lineno       Current line number.
 * @param[in] indentWidth  Width, in spaces, of one indent.
 * @param[in] ch           Last typed character.
 * @return                 New indent width.
 */
function processLine(line) {
    var result = -1;

    if (line.ch === '\n') {
        // try to insert comments
        tryComment(line);
    }

    // try different indentation triggers
    result = tryAlignOpenBrace(line);
    if (result === -1) {
        dbg('processLine: tryAlignOpenBrace failed');
        result = trySelect(line);
    }

    if (result === -1) {
        dbg('processLine: trySelect failed');
        result = tryKeywords(line);
    }

    if (result === -1) {
        dbg('processLine: tryKeywords failed');
        result = tryContinuation(line);
    }

    // finally, maybe we should unindent ...
    if (result === -1 && shouldUnindent(line.number)) {
        dbg('processLine: tryContinuation failed');
        result = line.prevIndent - line.indentWidth;
    }

    return result;
}

/**
 * @brief Add spaces around operators containing the equal sign.
 *
 * A couple of cases have to be checked:
 *  * if the previous character was =, /, < or >, we have already added spaces
 * around it, so we need to remove the last on and insert the equal there.
 *  * if the previous character was something else, we presume it's just a
 * simple = and add spaces around it.
 *
 * @param[in] cursor        Cursor.
 */
function tryEqualOperator(cursor) {
    var line = cursor.line,
        column = cursor.column;

    if (column < 2 || !cfgOperatorSpacing) {
        return;
    }

    var c = document.charAt(line, column - 2);
    switch (c) {
    case '=':
    case '/':
    case '>':
    case '<':
        addCharOrJumpOverIt(line, column, ' ');
        // Make sure there is a space before it!
        if (column >= 3 && document.charAt(line, column - 3) !== ' ') {
            document.insertText(line, column - 2, " ");
        }
        break;
    default:
        // always add a space around a single equal
        addCharOrJumpOverIt(line, column, ' ');
        document.insertText(line, column - 1, " ");
        break;
    }
}

/**
 * @brief Add spaces around a multiplication or power operator.
 *
 * In the case of a power operator **, we already added spaces around the first
 * star, so we check if it's there at column - 2.
 *
 * @param[in] cursor        Cursor.
 */
function tryPowerOperator(cursor) {
    var line = cursor.line,
        column = cursor.column;

    dbg('tryPowerOperator: start')
    if (column < 2 || !cfgOperatorSpacing) {
        return;
    }

    // we could have "* *" right now, so we got to check column - 2
    var c = document.charAt(line, column - 2);
    for (var i = 0; i < 4; i += 1) {
        dbg('tryPowerOperator: at c - ' + i + ' <' + document.charAt(line, column - i) + '>');
    }

    // if there is a * before the one we just enter, assume power operator
    if (c === '*') {
        addCharOrJumpOverIt(line, column, ' ');
        if (column >= 3 && document.charAt(line, column - 3) !== ' ') {
            document.insertText(line, column - 3, " ");
        }
    } else {
        // otherwise just add spaces around the *
        addCharOrJumpOverIt(line, column, ' ');
        document.insertText(line, column - 1, " ");
    }
}

/**
 * @brief Align preprocessor directives on column 0.
 *
 * @param[in] cursor        Cursor.
 */
function tryPreprocessor(cursor) {
    if (justEnteredCharIsFirstOnLine(cursor.line, cursor.column, '#')) {
        return 0;
    }

    return -1;
}

/**
 * @brief Add a space after every comma.
 *
 * @param[in] cursor        Cursor at current (line, column).
 */
function tryComma(cursor) {
    if (!cfgOperatorSpacing) {
        return;
    }

    addCharOrJumpOverIt(cursor.line, cursor.column, ' ');
}

/**
 * @brief Add spaces around a double colon operator.
 *
 * @param[in] cursor        Cursor.
 */
function tryDoubleColon(cursor) {
    var line = cursor.line,
        column = cursor.column;

    if (!cfgOperatorSpacing) {
        return;
    }

    dbg('tryDoubleColon: start <' + document.charAt(line, column - 2) + '>')
    // if the previous character is also a ':', add spaces around them
    if (document.charAt(line, column - 2) === ':') {
        addCharOrJumpOverIt(line, column, ' ');
        if (column >= 3 && document.charAt(line, column - 3) !== ' ') {
            document.insertText(line, column - 2, " ");
        }
    }
}

/**
 * @brief Add spaces around operators.
 *
 * Supported operators are: `+`, `-`, `*`, `/`, `=`, `<`, `>`.
 *
 * Operators that appear in other two-character operators:
 *  * `*` can appear as the power operator `**` as well.
 *  * `=` appears in the operators: `==`, `/=`, `>=`, '<='.
 *
 * @param[in] cursor        Cursor.
 * @param[in] ch            Character from an operator.
 */
function tryOperator(cursor, ch) {
    var line = cursor.line,
        column = cursor.column;

    if (!cfgOperatorSpacing) {
        return;
    }

    switch (ch) {
    case '=':
        tryEqualOperator(cursor);
        break;
    case '*':
        tryPowerOperator(cursor);
        break;
    default:
        // for the other operators, just add spaces around them
        addCharOrJumpOverIt(line, column, ' ');
        document.insertText(line, column - 1, " ");
        break;
    }
}

function processChar(lineno, ch) {
    var cursor = view.cursorPosition(),
        result = -1;

    dbg('processChar: start <' + ch + '>');
    if (isStringOrComment(cursor.line, cursor.column)) {
        return result;
    }

    document.editBegin();

    // Check if char under cursor is the same as just entered,
    // and if so, remove it... to make it behave like "overwrite" mode
    dbg('processChar: charAt cursor <' + document.charAt(cursor) + '>');
    if (ch !== ' ' && document.charAt(cursor) === ch) {
        document.removeText(lineno, cursor.column, lineno, cursor.column + 1);
    }

    switch (ch) {
    case '#':
        result = tryPreprocessor(cursor);
        break;
    case ',':
        tryComma(cursor);
        break;
    case ':':
        tryDoubleColon(cursor);
        break;
    case '+':
    case '-':
    case '*':
    case '/':
    case '=':
    case '>':
    case '<':
        tryOperator(cursor, ch);
        break;
    default:
        break;
    }

    document.editEnd();
    return result;
}

/**
 * @brief Compute the indentation width for a given line.
 *
 * Special values for indent width:
 * -2          No indent
 * -1          Keep previous indent
 *
 * @param[in] lineno       Current line number.
 * @param[in] indentWidth  Width, in spaces, of one indent.
 * @param[in] ch           Last typed character.
 * @return                 New indent width.
 */
function indent(lineno, indentWidth, ch) {
    // don't ever indent the first line
    if (lineno === 0) {
        return -2;
    }

    // all functions assume valid lineno on entry
    if (lineno < 0) {
        return -1;
    }

    var t = document.variable("debugMode");
    if (t) {
        debugMode = /^(true|on|enabled|1)$/i.test(t);
    }

    // get some commonly used stuff
    var result = -1;
    var line = new LineContext(lineno, indentWidth, ch);
    line.debug();

    // handle trigger characters
    if (ch !== '\n' && ch !== "") {
        result = processChar(lineno, ch);
    } else {
        result = processLine(line);
    }

    dbg('final indentation <' + result + '>');
    return result;
}

// kate: space-indent on; indent-width 4; replace-tabs on;
