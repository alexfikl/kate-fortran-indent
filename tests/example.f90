! Copyright (C) 2015 Alex Fikl <alexfikl@gmail.com>
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Lesser General Public
! License as published by the Free Software Foundation; either
! version 2.1 of the License, or (at your option) any later version.
!
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with this library. If not, write to the Free Software Foundation,
! Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

#include "something.h"

!> @brief This module contains some examples of things.
!> @details
!! It is used to test a fortran indentation script for kate
!! and all of this should look nice.
!!
!! very very nice indeed.
module test
    implicit none

    ! some type
    ! very complicated type indeed.
    type :: some_type_t
        integer :: a

    contains

        subroutine printf(this)
            type(some_type_t) :: this

            print*, this%a
            return
        end subroutine printf
    end type some_type_t

    type, public :: some_public_type_t
        integer :: b
    end type some_public_type_t

    type, abstract :: some_abstract_type_t
        integer :: c
    end type some_abstract_type_t

    interface
        subroutine some_interface_i(a)
            integer :: a
        end subroutine
    end interface

    abstract interface
        subroutine some_other_interface_i(a)
            logical :: a
        end subroutine
    end interface

contains

    subroutine test_if(a)
        implicit none

        integer :: a

        if (a == 0) then
            print*, 'a is 0'
        else if (a == 1) then
            print*, 'a is 1'
        else
            print*, 'a is something else'
        end if

        return
    end subroutine test_if

    subroutine test_do(a)
        implicit none

        integer :: a
        integer :: i

        do i = 1, a
            print*, i
        end do

        return
    end subroutine test_do

    subroutine test_select(a)
        implicit none

        integer :: a
        type(some_abstract_type_t) :: b

        select case (a)
        case (0)
            print*, "a is 0"
        case (1)
            print*, "a is 1"
        case default
            print*, "a is something else"
        end select

        select type (c => b)
        type is (some_abstract_type_t)
            print*, "b is some_abstract_type_t"
        class default
            print*, "who knows what b is"
        end select

        return
    end subroutine test_select

    subroutine test_where(a)
        implicit none

        integer, dimension(10) :: a
        integer, dimension(10) :: b

        b = -1
        a = (/ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 /)

        forall(i = 1, size(a, 1))
            b(i) = a(i) + 2
        end forall

        where (a > 5)
            b = a
        elsewhere
            b = -1
        end where

    end subroutine test_where

    subroutine test_continuation(a)
        implicit none

        integer :: a

        a = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 +     &
            9 + 10 + 11 + 12 + 13 + 14 + 15 +   &
            16

        a = some_function(with, many, many, arguments,  &
                          that, span, several, lines,   &
                          like this)

        if (some_weird_condition.eqv..true. .or. &
            some_other_condition.eq.7) then
            print*, 'wee'
        end if

    end subroutine test_continuation

    pure function test_pure(a) result(b)
        implicit none

        integer :: a
        type(some_type_t) :: b

    end function test_pure

    pure integer function test_pure_int(a)
        implicit none

        integer :: a
        type(some_type_t) :: b

    end function test_pure

    elemental function test_elemental(a) result(b)
        implicit none

        integer :: a
        type(some_type_t) :: b

    end function test_elemental

    recursive function test_recursive(a) result(b)
        implicit none

        integer :: a
        type(some_type_t) :: b

        if (abs(a) > 0) then
            b = test_recursive(abs(a) - 1) + 1
        end if

        return
    end function test_recursive

end module
