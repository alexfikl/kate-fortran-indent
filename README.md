# Kate Fortran Indenter

This is a Fortran indenter script for [Kate](https://projects.kde.org/projects/kde/applications/kate),
the KDE text editor. It was only tested with the frameworks-based version of
Kate (more specifically with version 14.12.0).

# Features

On the indentation side, the script:

* is aware of most (soon all) Fortran 90+ keywords and can indent them properly.
* has support for continuations using `&`. If the continuation is part of a
function call (or generally between parantheses), the next line is indented
to the last open paranthesis. If the continuation is part of a normal line,
nothing is done, but if the user chooses an indentation level, subsequent
continued lines will respect it.
* can indent (or not) the cases inside a select statement.
* certain keywords, such as `module` or `program` are always set to column 0.
* preprocessor commands are also set to column 0.
* the `contains` keyword can be indented at the same level as the parent
`module` or `subroutine` or at the same level as their content.
* comments are continued when the user starts a new line.

It has other small features, specifically related to operator spacing:

* adds spaces before and after common operators: `*`, `+`, `-`, `/`, `=`, etc.
* also supports the power operator `**` and various other two character
comparison operators.
* adds spaces after a comma.
* adds spaces around `::`.

# TODO

* loads of testing
* simplify it a little, maybe?
* use it for a couple of weeks!
* did Kate get configuration modules for indent scripts? Probably not..
